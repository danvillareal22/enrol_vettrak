<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * vettrak enrolment plugin settings and presets.
 *
 * @package    enrol_vettrak
 * @copyright  2010 Petr Skoda {@link http://skodak.org}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

if ($ADMIN->fulltree) {

    $settings->add(new admin_setting_heading('enrol_vettrak_defaults',
        get_string('enrolinstancedefaults', 'admin'), get_string('enrolinstancedefaults_desc', 'admin')));

    $settings->add(new admin_setting_configcheckbox('enrol_vettrak/defaultenrol',
        get_string('defaultenrol', 'enrol'), get_string('defaultenrol_desc', 'enrol'), 1));

    $options = array(ENROL_INSTANCE_ENABLED  => get_string('yes'),
                     ENROL_INSTANCE_DISABLED => get_string('no')
    );

    $settings->add(new admin_setting_configselect('enrol_vettrak/status',
        get_string('status', 'enrol_vettrak'), get_string('status_desc', 'enrol_vettrak'), ENROL_INSTANCE_ENABLED, @$options));

    $settings->add(new admin_setting_configtext('enrol_vettrak/allowedProgrammes', 'Allowed Program IDs (comma separated)','', ''));

    $settings->add(new admin_setting_configtext('enrol_vettrak/allowedEnrolmentStatuses', 'Allowed Enrolment Statuses','', 'Active (Commencement),Active'));
    $settings->add(new admin_setting_configtext('enrol_vettrak/rejectedEnrolmentStatuses', 'Rejected Enrolment Statuses','', 'Cancelled,Expired,No Show'));

    $settings->add(new admin_setting_configcheckbox('enrol_vettrak/useStartDate', 'Use Vettrak Enrolment Start Date', '', 1));
    $settings->add(new admin_setting_configcheckbox('enrol_vettrak/useEndDate', 'Use Vettrak Enrolment End Date', '', 1));

}
