<?php

namespace enrol_vettrak\task;

class fetch_programmes extends \core\task\scheduled_task {      
    public function get_name() {
        return get_string('fetch_programmes', 'enrol_vettrak');
    }
                                                                     
    public function execute() {
    	global $CFG;
    	require_once($CFG->dirroot . '/enrol/vettrak/lib.php');
    	$vettrak_enrol = enrol_get_plugin('vettrak');
		$vettrak_enrol->fetchProgrammes(true); // True to Rebuild Cache
    }                                                                                                     
} 