<?php

namespace enrol_vettrak\task;

class fetch_enrolments extends \core\task\scheduled_task {      
    public function get_name() {
        return get_string('fetch_enrolments', 'enrol_vettrak');
    }
                                                                     
    public function execute() {
    	$vettrak_enrol = enrol_get_plugin('vettrak');
		$vettrak_enrol->sync_enrolments2();
    }                                                                                                                               
} 