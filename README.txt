Instructions for installing Vettrak Enrolment on Moodle:

================================================================================


Please note: Before starting you need to install and configure the Vettrak Auth plugin:

https://git.ecreators.com.au/tim/auth_vettrak


================================================================================



Step 1: Install the plugin (use master branch)

You can do this by either downloading the plugin and uploading it into the 'enrol' folder inside your Moodle site or using CLI to clone this git repo into the 'enrol' folder.




Step 2: Upgrade the site

After you install the plugin you will need to upgrade the site. This can be done through the frontend by navitgating to the 'notifications' under site administration or running 'php admin/cli/upgrade.php' from the moodle root code directory using CLI.




Step 3: Configure the plugin

The auth plugin has all the required connection details. If you want to customise the settings go to:

'Site adminisitration -> Plugins -> Enrolments -> Vettrak Enrolment'.




Step 4: Check the sync is running

Navigate to 'Site administration -> Server -> Scheduled tasks' and look for the following tasks:


\enrol_vettrak\task\fetch_enrolments
\enrol_vettrak\task\fetch_occurrences
\enrol_vettrak\task\fetch_programmes
\enrol_vettrak\task\fetch_qualifications


This will tell you if it has run, when it is going to run and how often it will run. 

For testing purposes you can run the scheduled task from CLI using the following command from the root of the Moodle code:

'php admin/tool/task/cli/schedule_task.php --execute=(Enter one of the above with double '\\' between each part)'

This will also give you an error log.


================================================================================


Grade Sync:


To set up the grade sync please refer to:

https://ecreators.zendesk.com/hc/en-us/articles/210803303-Configuring-Vettrak-Grade-Synchronisation

Confirm the following scheduled task is running (be default it is off):

\enrol_vettrak\task\push_grades

This should sync grades back to Vettrak if set up properly