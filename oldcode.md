    // public static function course_completed($object) {

    //     mtrace('TODO!');

    //     global $DB, $CFG;
    //     require_once($CFG->dirroot . '/auth/vettrak/auth.php');
    //     require_once($CFG->libdir . '/gradelib.php');
    //     require_once($CFG->dirroot . '/grade/querylib.php');
    //     $client = auth_plugin_vettrak::vettrak_client();
    //     $token = auth_plugin_vettrak::vettrak_get_token($client);
    //     $COURSE = get_course($object->course);
    //     $USER = $DB->get_record('user', array('id' => $object->userid));
    //     $finalgrade = grade_get_course_grade($USER->id, $COURSE->id);
    //     $UserDetails = auth_plugin_vettrak::GetClientExtendedDetails($USER->idnumber);
    //     foreach ($UserDetails['GetClientExtendedDetailsResult']['ClieExtended']['EnrolledUnits']['TEnun'] as $EnrolmentUnit) {
            
    //         if ($EnrolmentUnit['Unit_Code'] !== $COURSE->shortname) {
    //             continue;
    //         }

    //         $timeenrolled = new DateTime();
    //         $timeenrolled->setTimestamp($object->timeenrolled);
    //         $timeenrolled = $timeenrolled->format('c');

    //         $timecompleted = new DateTime();
    //         $timecompleted->setTimestamp($object->timecompleted);
    //         $timecompleted = $timecompleted->format('c');

    //         $UpdateEnrolment = array(
    //             'sToken' => $token,
    //             'iEnro_ID' => $EnrolmentUnit['ID'], // Unique to the user
    //             'sUnit_Code' => $COURSE->shortname,
    //             'sResult_Name' => get_string('sResult_Name', 'enrol_vettrak'),
    //             // 'sResult_Name' => 'Competent',
    //             'xsdStart' => $timeenrolled,
    //             'xsdFinish' => $timecompleted,
    //             'iPercentage' => round($finalgrade->grade, 0)
    //         );

    //         $x = $client->call('UpdateResult', $UpdateEnrolment);

    //         if ((isset($x['UpdateResultResult']['StatusMessage'])) && ($x['UpdateResultResult']['StatusMessage'] == 'Result Recorded.')) {
    //             return true;
    //         } else {
    //             return false; // Try again later...
    //         }
            
    //         return false;   
    //     }
    //     return false;
    // }


    public function getDivisions() {

        $additionaldata = $this->client->call('QueryAdditionalData', array(
                'token' => $this->token,
                'entityName' => 'Employer'
                // 'filterCriteria' => array(
                //     'Field' => 'QualificationCode',
                //     'Operator' => 'Equals',
                //     'Value' => $qualificationcode
                // )
            )
        );

        die(print_r($additionaldata));


        // die('here');
        $divisions = $this->client->call("GetDivisions",
            array(
                'token' => $this->token,
                // 'divisionId' => 2
            )
        );

        die(print_r($divisions));


    }

    public function activityCompletion($user, $activityid, $grade) {

        global $CFG, $DB;

        require_once($CFG->dirroot . '/auth/vettrak/auth.php');

        $this->client = auth_plugin_vettrak::vettrak_client();

        $this->token = auth_plugin_vettrak::vettrak_get_token($this->client);

        $tmpClient = new SoapClient(auth_plugin_vettrak::vettrak_webservice());
        $additionaldata = $tmpClient->QueryAdditionalData(
                        array(
                            'token' => $this->token, // QueryAdditionalData - EnrolledUnit
                            'entityName' => 'EnrolledUnit',
                            'filterCriteria' => array(
                                                    array(
                                                        'Field' => 'UnitCode',
                                                        'Operator' => 'Equals',
                                                        'Value' => $activityid
                                                    ),
                                                    array(
                                                        'Field' => 'ClientCode',
                                                        'Operator' => 'Equals',
                                                        'Value' => $user->idnumber
                                                    )
                            )
                        )
        );

        $additionaldata = json_decode(json_encode($additionaldata), true);

        $additionaldata = $this->parseAdditionalData($additionaldata);

        $msg = print_r($additionaldata, true);
        $url = 'https://api.hipchat.com/v1/rooms/message?auth_token=fc69aef3b4b72aa0a699b06a0d8f3d&color=green&room_id=1510968&from=vettrak&message_format=text&message=' . $msg;
        file_get_contents($url);

        if (count($additionaldata != 1)) {

            $msg = 'Unable to isolate the Enrolled Unit ' . print_r($additionaldata, true);
            $url = 'https://api.hipchat.com/v1/rooms/message?auth_token=fc69aef3b4b72aa0a699b06a0d8f3d&color=green&room_id=1510968&from=vettrak&message_format=text&message=' . $msg;
            file_get_contents($url);

            return false;
        }

        die('activityCompletion');

    }