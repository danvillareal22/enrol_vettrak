<?php

define('CLI_SCRIPT', true);

require(dirname(dirname(dirname(dirname(__FILE__)))).'/config.php'); // global moodle config file.

$CFG->debug = 32767;
$CFG->debugdisplay = true;

require_once($CFG->dirroot . '/lib/grade/grade_item.php');
require_once($CFG->dirroot . '/lib/grade/grade_grade.php');
require_once($CFG->dirroot . '/lib/grade/grade_scale.php');

require_once($CFG->dirroot . '/auth/vettrak/auth.php');

$grades = $DB->get_records_sql("        SELECT      GG.id,
                                                U.username,
                                                U.idnumber,
                                                U.firstname,
                                                U.lastname,
                                                C.shortname,
                                                GI.id as 'itemid',
                                                GG.id as 'gradeid',
                                                GI.iteminfo,
                                                GG.finalgrade,
                                                GG.timemodified,
                                                (SELECT MIN(timestart) FROM mdl_user_enrolments UE, mdl_enrol E WHERE E.courseid = C.id AND UE.enrolid = E.id and UE.userid = U.id GROUP BY E.id) as 'minEnrolment',
                                                (SELECT finalgrade FROM mdl_grade_grades GG2, mdl_grade_items GI2 WHERE GI2.itemtype = 'course' AND GG2.itemid = GI2.id AND GG2.userid = U.id) as 'coursePercentage'
                                        FROM mdl_grade_items GI, mdl_grade_grades GG, mdl_user U, mdl_course C
                                        WHERE GI.iteminfo IS NOT NULL AND GI.iteminfo != ''
                                        AND GG.itemid = GI.id
                                        AND GG.userid = U.id
                                        AND GI.courseid = C.id
                                        AND GG.exported = GG.exported
                                        AND U.idnumber != ''
                                        AND GG.finalgrade IS NOT NULL
                                        AND U.auth = 'vettrak'");

$client = auth_plugin_vettrak::vettrak_client();

$token = auth_plugin_vettrak::vettrak_get_token($client);

$tmpClient = new SoapClient(auth_plugin_vettrak::vettrak_webservice());

foreach ($grades as $grade) {

    $gradeitem = new grade_item(array('id' => $grade->itemid));
    $gradegrade = new grade_grade(array('id' => $grade->gradeid));
    $scale = grade_scale::fetch(array('id' => $gradeitem->scaleid));
    $grade->scalevalue = $scale->get_nearest_item($gradegrade);
    $values = explode(',',$scale->scale);
    $grade->scalevalue = trim($values[(int) $grade->finalgrade -1]); // MOODLE I WANT TO USE YOUR API BUT NOTHING MAKES SENSE

    $ClientEnrolment = $tmpClient->QueryAdditionalData(
        array(
            'token' => $token, // QueryAdditionalData - EnrolledUnit
            'entityName' => 'EnrolledUnit',
            'filterCriteria' => array(
                array(
                    'Field' => 'ClientCode',
                    'Operator' => 'Equals',
                    'Value' => $grade->idnumber
                ),
                array(
                    'Field' => 'UnitCode',
                    'Operator' => 'Equals',
                    'Value' => $grade->iteminfo
                )
            )
        )
    );

    if ($ClientEnrolment->QueryAdditionalDataResult->Auth->StatusMessage == '0 records returned') {

        mtrace('Unable to identify enrolment record for ClientCode: ' . $grade->idnumber . ' (' . "$grade->firstname $grade->lastname" . '), UnitCode: ' . $grade->iteminfo);
    } else if ($ClientEnrolment->QueryAdditionalDataResult->Auth->StatusMessage == '1 records returned') {

        if(!empty($grade->locktime)){

        }elseif(!empty($grade->timemodified)){
            $timecompleted = DateTime::createFromFormat('U', $grade->timemodified, new DateTimeZone('Australia/Sydney'));
            $timecompleted = $timecompleted->format('c');
        } else {
            $timecompleted = DateTime::createFromFormat('U', time(), new DateTimeZone('Australia/Sydney'));
            $timecompleted = $timecompleted->format('c');
        }

        if (!empty($grade->minEnrolment)) {
            $timeenrolled = DateTime::createFromFormat('U', $grade->minEnrolment, new DateTimeZone('Australia/Sydney'));
            $timeenrolled = $timeenrolled->format('c');
        } else {
            $timeenrolled = DateTime::createFromFormat('U', $grade->timemodified - 60, new DateTimeZone('Australia/Sydney'));
            $timeenrolled = $timeenrolled->format('c');
        }

        $percentage = (int) $grade->coursePercentage;

        $UpdateResult = array(
            'sToken' => $token,
            'iEnro_ID' => $ClientEnrolment->QueryAdditionalDataResult->Values->ArrayOfString->string[2],
            'sUnit_Code' => $ClientEnrolment->QueryAdditionalDataResult->Values->ArrayOfString->string[0],
            'sResult_Name' => $grade->scalevalue,
            'xsdStart' => $timeenrolled,
            'xsdFinish' => $timecompleted,
            'iPercentage' => round($percentage, 0)
        );

        $UpdateResultResponse = $ClientEnrolment = $tmpClient->UpdateResult($UpdateResult);

        if ($UpdateResultResponse->UpdateResultResult->StatusMessage == 'Result Recorded.') {
            mtrace('Successfully updated Enrolment Result: ' . $grade->idnumber . ' (' . "$grade->firstname $grade->lastname" . '), UnitCode: ' . $grade->iteminfo . ', sResult_Name: ' . $grade->scalevalue . ', xsdStart: ' . $timeenrolled . ', xsdFinish: ' . $timecompleted . ', iPercentage: ' . round($percentage, 0));
        } else {
            mtrace('Error updating Enrolment Result: ' . $grade->idnumber . ' (' . "$grade->firstname $grade->lastname" . '), UnitCode: ' . $grade->iteminfo . ', sResult_Name: ' . $grade->scalevalue . ', xsdStart: ' . $timeenrolled . ', xsdFinish: ' . $timecompleted . ', iPercentage: ' . round($percentage, 0));
        }

    } else {
        die('Many Enrolment Records Found? What do?');
    }

}



