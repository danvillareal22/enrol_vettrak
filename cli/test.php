<?php

define('CLI_SCRIPT', true);

require(dirname(dirname(dirname(dirname(__FILE__)))).'/config.php'); // global moodle config file.

require_once($CFG->dirroot . '/auth/vettrak/auth.php');

// die('here');

$client = auth_plugin_vettrak::vettrak_client();

$token = auth_plugin_vettrak::vettrak_get_token($client);
$tmpClient = new SoapClient(auth_plugin_vettrak::vettrak_webservice());

$object = array(
    'token' => $token,
    'entityName' => 'Client',
    'filterCriteria' => array(
    	array(
    		'Field' => 'GivenName',
    		'Operator' => 'Equals',
    		'Value' => 'Ryan',
    	),
    	array(
    		'Field' => 'Surname',
    		'Operator' => 'Equals',
    		'Value' => 'Heaslip1',
    	)
    )
);

$clients = $tmpClient->QueryAdditionalData($object, array("trace" => 1, "exception" => 1, 'compression' => SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP));

$data = array();

if ($clients->QueryAdditionalDataResult->Auth->StatusMessage == '0 records returned') {
	// Nothig
} else if ($clients->QueryAdditionalDataResult->Auth->StatusMessage == '1 records returned') {
	$fields = $clients->QueryAdditionalDataResult->Fields->string;
	$c_data = $clients->QueryAdditionalDataResult->Values->ArrayOfString->string;
	$client = new stdClass();
	foreach ($c_data as $fkey => $field) {
		$client->{$fields[$fkey]} = $field;
	}
	$data[$client->ClientCode] = $client;
} else {
	$fields = $clients->QueryAdditionalDataResult->Fields->string;
	$c_data_array = $clients->QueryAdditionalDataResult->Values->ArrayOfString;
	foreach ($c_data_array as $c_data) {
		$c_data = $c_data->string;
		$client = new stdClass();
		foreach ($c_data as $fkey => $field) {
			$client->{$fields[$fkey]} = $field;
		}
		$data[$client->ClientCode] = $client;
	}
}

print_r($data);





