<?php

define('CLI_SCRIPT', true);

require(dirname(dirname(dirname(dirname(__FILE__)))).'/config.php'); // global moodle config file.

require_once($CFG->dirroot . '/auth/vettrak/auth.php');

$client = auth_plugin_vettrak::vettrak_client();
$token = auth_plugin_vettrak::vettrak_get_token($client);
$tmpClient = new SoapClient(auth_plugin_vettrak::vettrak_webservice());

$vettrak_enrol = enrol_get_plugin('vettrak');

$options = $vettrak_enrol->fetchOccurrenceOptions(true);

print_r($options);

die();

// $programmes = $vettrak_enrol->fetchProgrammes(true);
// $qualifications = $vettrak_enrol->fetchQualifications(true);

// $enrolmentFields = $tmpClient->GetAdditionalDataFieldsForEntity(array(
// 	'token' => $token,
// 	'entityName' => 'Enrolment'
// ));

// foreach ($programmes as $program) {

// 	if ($program['Prog_ID'] != 126) {
// 		continue;
// 	}

	// $additionaldata = $tmpClient->QueryAdditionalData(
 //        array(
 //            'token' => $token, // QueryAdditionalData - EnrolledUnit
 //            'entityName' => 'Enrolment',
 //            'filterCriteria' => array(
 //                array(
 //                    'Field' => 'QualificationCode',
 //                    'Operator' => 'Equals',
 //                    'Value' => 'CPC40110'
 //                ),
 //                array(
 //                    'Field' => 'ClientCode',
 //                    'Operator' => 'Equals',
 //                    'Value' => ''
 //                ),
 //            )
 //        )
	// );

	// print_r($additionaldata);
// }

// print_r($programmes);

// $occurrenceDetails = $tmpClient->GetOccurrenceDetails(array(
//         'sToken' => $token,
//         'iOccu_ID' => '73137'
//     )
// );

// print_r($occurrenceDetails);


//$vettrak_enrol->fetchProgrammes(true);
//$vettrak_enrol->fetchQualifications(true); // True to Rebuild Cache
// $vettrak_enrol->fetchOccurrenceOptions(true); // True to Rebuild Cache
//$vettrak_enrol->sync_enrolments2();
