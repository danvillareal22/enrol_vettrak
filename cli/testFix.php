<?php

define('CLI_SCRIPT', true);

require(dirname(dirname(dirname(dirname(__FILE__)))).'/config.php'); // global moodle config file.

$vettrak_enrol = enrol_get_plugin('vettrak');
$programmes = $vettrak_enrol->fetchProgrammes(true);

foreach ($programmes as $programme) {

	if ($programme['Prog_ID'] == 126) {
		print_r($programme);
	}

}