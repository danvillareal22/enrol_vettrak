<?php

class adhoc_sync_enrolments extends \core\task\adhoc_task {                                                                           
    public function execute() {       
        $vettrak_enrol = enrol_get_plugin('vettrak');
        $vettrak_enrol->sync_enrolments2();
    }                                                                                                                               
}

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/enrol/vettrak/lib/nusoap-0.9.5/lib/nusoap.php');
require_once($CFG->dirroot . '/auth/vettrak/auth.php');

/* Query Additional Data Models
    [0] => Client
    [1] => Employer
    [2] => Enrolment
    [3] => Contract
    [4] => Class
    [5] => ClassAttendance
    [6] => ClientEvent
    [7] => EmployerEvent
    [8] => ClientAward
    [9] => EmployerContact
    [10] => EnrolledUnit
*/

function enrol_vettrak_course_completed($object) { // Stub Function. No Longer Used.
    return;
}

function enrol_vettrak_user_graded($object) { // Stub Function. No Longer Used.
    return;
}

class enrol_vettrak_plugin extends enrol_plugin {

    public function __construct() {

    }

    public function fetchUnits() {

        $this->tmpClient = new SoapClient(
            auth_plugin_vettrak::vettrak_webservice(),
            array(
                "trace" => true,
                'cache_wsdl' => WSDL_CACHE_NONE
            )
        );

        $this->client = auth_plugin_vettrak::vettrak_client();
        $this->token = auth_plugin_vettrak::vettrak_get_token($this->client);
        $tmpClient = $this->tmpClient;

        mtrace('1');

        $data = array(
            'sToken' => $this->token,
            'iID' => '69987'
        );

        print_r($data);

        $units = $tmpClient->GetUnitsForOccurrence($data);

        print_r($units);

        die('here');
        // GetUnitsForOccurrence
    }

    public function sync_gradeitems() {

        global $CFG, $DB;

        require_once($CFG->dirroot . '/lib/grade/grade_item.php');
        require_once($CFG->dirroot . '/lib/grade/grade_grade.php');
        require_once($CFG->dirroot . '/lib/grade/grade_scale.php');

        $this->tmpClient = new SoapClient(
            auth_plugin_vettrak::vettrak_webservice(),
            array(
                "trace" => true,
                'cache_wsdl' => WSDL_CACHE_NONE
            )
        );


        $grades = $DB->get_records_sql("        SELECT  GG.id,
                                                        U.username,
                                                        U.id as 'userid',
                                                        U.idnumber,
                                                        U.firstname,
                                                        U.lastname,
                                                        C.shortname,
                                                        GI.id as 'itemid',
                                                        GG.id as 'gradeid',
                                                        GI.iteminfo,
                                                        GG.finalgrade,
                                                        GG.timemodified,
                                                       (SELECT MIN(timestart) FROM mdl_user_enrolments UE, mdl_enrol E WHERE E.enrol = 'vettrak' and E.courseid = C.id AND UE.enrolid = E.id and UE.userid = U.id GROUP BY E.id) as 'minEnrolment',
                                                       (SELECT finalgrade FROM mdl_grade_grades GG2, mdl_grade_items GI2 WHERE GI2.itemtype = 'course' AND GG2.itemid = GI2.id AND GG2.userid = U.id AND GI2.courseid = C.id) as 'coursePercentage'
                                                FROM mdl_grade_items GI, mdl_grade_grades GG, mdl_user U, mdl_course C
                                                WHERE GI.iteminfo IS NOT NULL AND GI.iteminfo != ''
                                                AND GG.itemid = GI.id
                                                AND GG.userid = U.id
                                                AND GI.courseid = C.id
                                                AND U.idnumber != ''
                                                AND GI.gradetype = 2
                                                AND GG.finalgrade IS NOT NULL
                                                AND U.id NOT IN (SELECT UP.userid FROM mdl_user_preferences UP WHERE UP.userid = U.id AND UP.name = CONCAT('VT-', GG.id) AND UP.value = GG.timemodified)
                                                AND U.auth IN ('vettrak')");

        $this->client = auth_plugin_vettrak::vettrak_client();
        $this->token = auth_plugin_vettrak::vettrak_get_token($this->client);
        $tmpClient = $this->tmpClient;

        foreach ($grades as $grade) {

            $gradeitem = new grade_item(array('id' => $grade->itemid));
            $gradegrade = new grade_grade(array('id' => $grade->gradeid));
            $scale = grade_scale::fetch(array('id' => $gradeitem->scaleid));
            $grade->scalevalue = $scale->get_nearest_item($gradegrade);
            $values = explode(',',$scale->scale);
            $grade->scalevalue = trim($values[(int) $grade->finalgrade -1]); // MOODLE I WANT TO USE YOUR API BUT NOTHING MAKES SENSE
            $ClientEnrolment = $tmpClient->QueryAdditionalData(
                array(
                    'token' => $this->token, // QueryAdditionalData - EnrolledUnit
                    'entityName' => 'EnrolledUnit',
                    'filterCriteria' => array(
                        array(
                            'Field' => 'ClientCode',
                            'Operator' => 'Equals',
                            'Value' => $grade->idnumber
                        ),
                        array(
                            'Field' => 'UnitCode',
                            'Operator' => 'Equals',
                            'Value' => $grade->iteminfo
                        )
                    )
                )
            );

            if ($ClientEnrolment->QueryAdditionalDataResult->Auth->StatusMessage == '0 records returned') {
                mtrace('Unable to Identify Enrolment in Vettrak');
            } else if ($ClientEnrolment->QueryAdditionalDataResult->Auth->StatusMessage == '1 records returned') {

                mtrace('Syncing Grade for userid: '. $grade->userid);

                if(!empty($grade->locktime)){

                }elseif(!empty($grade->timemodified)){
                    $timecompleted = DateTime::createFromFormat('U', $grade->timemodified, new DateTimeZone('Australia/Sydney'));
                    $timecompleted = $timecompleted->format('c');
                } else {
                    $timecompleted = DateTime::createFromFormat('U', time(), new DateTimeZone('Australia/Sydney'));
                    $timecompleted = $timecompleted->format('c');
                }

                if (!empty($grade->minEnrolment)) {
                    $timeenrolled = DateTime::createFromFormat('U', $grade->minEnrolment, new DateTimeZone('Australia/Sydney'));
                    $timeenrolled = $timeenrolled->format('c');
                } else {
                    $timeenrolled = DateTime::createFromFormat('U', $grade->timemodified - 60, new DateTimeZone('Australia/Sydney'));
                    $timeenrolled = $timeenrolled->format('c');
                }

                $percentage = (int) $grade->coursePercentage;

                $UpdateResult = array(
                    'sToken' => $this->token,
                    'iEnro_ID' => $ClientEnrolment->QueryAdditionalDataResult->Values->ArrayOfString->string[2],
                    'sUnit_Code' => $ClientEnrolment->QueryAdditionalDataResult->Values->ArrayOfString->string[0],
                    'sResult_Name' => $grade->scalevalue,
                    'xsdStart' => $timeenrolled,
                    'xsdFinish' => $timecompleted,
                    'iPercentage' => round($percentage, 0)
                );

                $UpdateResultResponse = $ClientEnrolment = $tmpClient->UpdateResult($UpdateResult);
                set_user_preference('VT-' . $grade->id, $grade->timemodified, $grade->userid);

            } else {
                mtrace('Duplicae vettrak UnitCode has been found in mutiple modules, not pushing any data. Problem needs to be fixed inside Vettrak');
            }

        }

    }

    public function fetchOccurrenceOptions($rebuildCache = false) {

        $cache = cache::make_from_params(cache_store::MODE_APPLICATION, 'enrol_vettrak', 'mycache');

        $this->tmpClient = new SoapClient(
            auth_plugin_vettrak::vettrak_webservice(),
            array(
                "trace" => true,
                'cache_wsdl' => WSDL_CACHE_NONE
            )
        );

        $data = $cache->get('fetchOccurrenceOptions');

        if ($data && ($rebuildCache != true)) {
            return $data;
        }

        global $CFG;

        $options = array();

        $CFG->enrol_vettrak_occurrence_skip_programs = false;

        if (!@$CFG->enrol_vettrak_occurrence_skip_programs) {

            $programmesoccurrences = self::fetchOccurrences();
            
            foreach ($programmesoccurrences as $po) {
                $programmename = $po['Prog_Code'] . ' ' . $po['Prog_Name'] . ' (';
                foreach ($po['Occurrences'] as $occu) {
                    $name = '';
                    if (!empty($occu->Code)) {
                        $name = $programmename . $occu->Code;
                    } else {
                        $name = $programmename . $occu->EnrolmentId;
                    }
                    $namestring = $name . ") - " . $occu->StartDate . " - " . $occu->EndDate;
                    $options[$occu->EnrolmentId] = $namestring;
                }
            }

        }

        $CFG->enrol_vettrak_occurrence_skip_qualifications = true;

        if (!@$CFG->enrol_vettrak_occurrence_skip_qualifications) {

            $this->client = auth_plugin_vettrak::vettrak_client();
            $this->token = auth_plugin_vettrak::vettrak_get_token($this->client);
            $tmpClient = $this->tmpClient;

            $qualifications = $tmpClient->GetQualifications(array('sToken' => $this->token));

            foreach ($qualifications->GetQualificationsResult->QualList->TQual as $qualification) {

                $QualificationOccurrences = $tmpClient->QueryAdditionalData(
                    array(
                        'token' => $this->token, // QueryAdditionalData - EnrolledUnit
                        'entityName' => 'Enrolment',
                        'filterCriteria' => array(
                            array(
                                'Field' => 'QualificationCode',
                                'Operator' => 'Equals',
                                'Value' => $qualification->Qual_Code
                            ),
                            array(
                                'Field' => 'ClientCode',
                                'Operator' => 'Equals',
                                'Value' => ''
                            )
                        )
                    )
                );

                if ($QualificationOccurrences->QueryAdditionalDataResult->Auth->StatusMessage == '1 records returned') {
                    $row = $QualificationOccurrences->QueryAdditionalDataResult->Values->ArrayOfString;
                    $QualificationOccurrences->QueryAdditionalDataResult->Values->ArrayOfString = array($row);
                } else if ($QualificationOccurrences->QueryAdditionalDataResult->Auth->StatusMessage == '0 records returned') {
                    continue;   
                }

                foreach ($QualificationOccurrences->QueryAdditionalDataResult->Values->ArrayOfString as $occu) {
                    $qualificationname = $qualification->Qual_Code . ' ' . $qualification->Qual_Name . ' (';
                    $occurrenceDetails = $tmpClient->GetOccurrenceDetails(array(
                            'sToken' => $this->token,
                            'iOccu_ID' => $occu->string['0']
                        )
                    );
                    if (!empty($occurrenceDetails->GetOccurrenceDetailsResult->Occu->Code)) {
                        $occuname = $qualificationname . $occurrenceDetails->GetOccurrenceDetailsResult->Occu->Code;
                    } else {
                        $occuname = $qualificationname . $occurrenceDetails->GetOccurrenceDetailsResult->Occu->EnrolmentId;
                    }
                    $eid = $occu->string['0'];
                    $occu_startdate = $occu->string['6'];
                    $occu_enddate = $occu->string['7'];
                    $namestring = $occuname . ") - " . $occu_startdate . " - " . $occu_enddate;
                    $options[$eid] = $namestring;
                }

            }
        }

        print_r($options);

        $cache->set('fetchOccurrenceOptions', $options);

        return $options;
    }

    public function fetchOccurrences() {

        $this->client = auth_plugin_vettrak::vettrak_client();
        $this->token = auth_plugin_vettrak::vettrak_get_token($this->client);
        $tmpClient = $this->tmpClient;

        $programmes = self::fetchProgrammes(true);

        foreach ($programmes as $pkey => $programme) {
            $occurrences = self::fetchOccurrencesForProgramme($programme['Prog_ID'], true);
            foreach ($occurrences as $okey => $occur) {
                //sleep(2);
    try {
        
                $occurrenceDetails = $tmpClient->GetOccurrenceDetails(array(
                        'sToken' => $this->token,
                        'iOccu_ID' => $occur->EnrolmentId
                    )
                );
    } catch (Exception $e) {
        mtrace("Skipping");
        continue;   
    }

                print_r($occur);
                if (isset($occurrenceDetails->GetOccurrenceDetailsResult->Occu->Code)) {
                    $occurrences[$okey]->Code = $occurrenceDetails->GetOccurrenceDetailsResult->Occu->Code;
                }

            }
            $programmes[$pkey]['Occurrences'] = $occurrences;
        }
        return $programmes;
    }

    public function fetchQualifications($rebuildCache = false) {

        $this->client = auth_plugin_vettrak::vettrak_client();
        $this->token = auth_plugin_vettrak::vettrak_get_token($this->client);
        $cache = cache::make_from_params(cache_store::MODE_APPLICATION, 'enrol_vettrak', 'misc');

        $qualifications = $cache->get('fetchQualifications');

        if ($qualifications && (!$rebuildCache)) {
            return $qualifications;
        }

        if ($rebuildCache) {
            $qualifications = $this->client->call('GetQualifications', array(
                    'sToken' => $this->token,
                )
            );
            $qualifications = $qualifications['GetQualificationsResult']['QualList']['TQual'];
            print_r($qualifications);
            $cache->set('fetchQualifications', $qualifications);
            return $qualifications;
        } else {
            return array();
        }
    }

    public function fetchProgrammes($rebuildCache = false) {

        $this->client = auth_plugin_vettrak::vettrak_client();
        $this->token = auth_plugin_vettrak::vettrak_get_token($this->client);
        $cache = cache::make_from_params(cache_store::MODE_APPLICATION, 'enrol_vettrak', 'misc');
        $programmes = $cache->get('fetchProgrammes');

        if ($programmes && (!$rebuildCache)) {
            return $programmes;
        }

        if ($rebuildCache) {

            mtrace('Rebuilding Programmes Cache');

            // Rebuilding Cache

            $programmes = $this->client->call('GetProgrammes', array(
                    'sToken' => $this->token,
                )
            );
            $programmes = $programmes['GetProgrammesResult']['ProgList']['TProg'];
            
            foreach ($programmes as $key => $prog) {
                if ($prog['Prog_Active'] == 1) {
                    unset($programmes[$key]);
                }
            }

            print_r($programmes);

            $config = get_config('enrol_vettrak');

            if (!empty($config->allowedProgrammes)) {
                $allowedProgrammes = explode(',', $config->allowedProgrammes);
            } else {
                $allowedProgrammes = array();
            }

            if (!empty($allowedProgrammes)) {
                foreach ($programmes as $pkey => $pvalue) {
                    if (!in_array($pvalue['Prog_ID'], $allowedProgrammes)) {
                        unset($programmes[$pkey]);
                    }
                }
            }

            $cache->set('fetchProgrammes', $programmes);
            return $programmes;
        } else {
            return array();
        }
    }

    public function fetchEntities() {

        $entities = $this->client->call('GetAdditionalDataEntityNames', array(
                'token' => $this->token,
            )
        );

        return $entities;

    }

    public function fetchEnrolmentsForOccurrence($occurrenceID, $courseid) {

        global $DB;

        $config = get_config('enrol_vettrak');

        // Fetch Occurrence Details -- Get the Code

        $tmpClient = $this->tmpClient;

        $occurrenceDetails = $tmpClient->GetOccurrenceDetails(array(
                'sToken' => $this->token,
                'iOccu_ID' => $occurrenceID
            )
        );

        $code = '';

        if (isset($occurrenceDetails->GetOccurrenceDetailsResult->Occu->Code)) {
            $code = $occurrenceDetails->GetOccurrenceDetailsResult->Occu->Code;
        } else {
            mtrace('Unable to Identify Occurrence for ID: ' . $occurrenceID);
            return;
        }

        $enrolments = $tmpClient->GetEnrolmentsForOccurrence(array(
                'sToken' => $this->token,
                'iID' => $occurrenceID
            )
        );

        $enrols = array();
        if (($enrolments->GetEnrolmentsForOccurrenceResult->Auth->Status == 1) && (@$enrolments->GetEnrolmentsForOccurrenceResult->Auth->StatusMessage != 'No Client Enrolments available.')) {
            if (is_array($enrolments->GetEnrolmentsForOccurrenceResult->ClieEnroList->TClieEnro)) {
                foreach ($enrolments->GetEnrolmentsForOccurrenceResult->ClieEnroList->TClieEnro as $enrol) {
                    $enrols[] = $enrol;
                }
            } else {
                if($enrolments->GetEnrolmentsForOccurrenceResult->ClieEnroList->TClieEnro) {
                    $enrols[] = $enrolments->GetEnrolmentsForOccurrenceResult->ClieEnroList->TClieEnro;
                }
            }
        }

        foreach ($enrols as $e) {
            $enrolment = new stdClass();
            $enrolment->clientcode = $e->Clie_Code;
            $enrolment->courseid = $courseid;

            if (!empty($e->StartDate)) {
                $startdate = DateTime::createFromFormat('Y-m-d\T00\:00\:00', $e->StartDate, new DateTimeZone('Australia/Sydney'));
                $startdate->modify('midnight');
                $enrolment->startdate = $startdate->getTimestamp();
            } else {
                $enrolment->startdate = 0;
            }

            if (!empty($e->EndDate)) {
                $enddate = DateTime::createFromFormat('Y-m-d\T00\:00\:00', $e->EndDate, new DateTimeZone('Australia/Sydney'));
                $enddate->modify('midnight');
                $enrolment->enddate = $enddate->getTimestamp();
            } else {
                $enrolment->enddate = 0;
            }

            $enrolment->status = $e->Status;
            $enrolment->occucode = $code;

            $allowedStatuses = explode(',', $config->allowedEnrolmentStatuses);
            $rejectedstatuses = explode(',', $config->rejectedEnrolmentStatuses);

            if (in_array($e->Status, $allowedStatuses)) {
                print_r($enrolment);
                try {
                    $DB->insert_record('tmp_enrol_vettrak', $enrolment);
                } catch (Exception $e) {
                    mtrace('Issue inserting record.');
                }
                
            } else if (in_array($e->Status, $rejectedstatuses)) {
                // mtrace("known bad status");
            } else {
                
            }
        }
    }

    public function sync_enrolments2() {

        global $CFG, $DB;

        require_once($CFG->dirroot . '/auth/vettrak/auth.php');
        require_once($CFG->dirroot . '/enrol/vettrak/lib.php');

        $this->tmpClient = new SoapClient(
            auth_plugin_vettrak::vettrak_webservice(),
            array(
                "trace" => true,
                'cache_wsdl' => WSDL_CACHE_NONE
            )
        );
        
        $this->client = auth_plugin_vettrak::vettrak_client();
        $this->token = auth_plugin_vettrak::vettrak_get_token($this->client);

        $dbman = $DB->get_manager();
        $table = new xmldb_table('tmp_enrol_vettrak');
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('clientcode', XMLDB_TYPE_CHAR, '100', null, XMLDB_NOTNULL, null, null);
        $table->add_field('courseid', XMLDB_TYPE_CHAR, '100', null, XMLDB_NOTNULL, null, null);
        $table->add_field('startdate', XMLDB_TYPE_CHAR, '100', null, XMLDB_NOTNULL, null, null);
        $table->add_field('enddate', XMLDB_TYPE_CHAR, '100', null, XMLDB_NOTNULL, null, null);
        $table->add_field('occucode', XMLDB_TYPE_CHAR, '100', null, XMLDB_NOTNULL, null, null);
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $dbman->create_temp_table($table);

        // $transaction = $DB->start_delegated_transaction();

        $occurrenceEnrolInstances = $DB->get_records('enrol', array('enrol' => 'vettrak', 'status' => 0, 'customchar2' => 'occurrence'));
        foreach ($occurrenceEnrolInstances as $occurrenceEnrolInstance) {
            $occurrenceIDs = explode(',', $occurrenceEnrolInstance->customchar1);
            foreach ($occurrenceIDs as $occurrenceID) {
                $this->fetchEnrolmentsForOccurrence($occurrenceID, $occurrenceEnrolInstance->courseid);
            }
        }

        $programmeEnrolInstances = $DB->get_records('enrol', array('enrol' => 'vettrak', 'status' => 0, 'customchar2' => 'programme'));

        foreach ($programmeEnrolInstances as $programmeEnrolInstance) {
            $occurrences = $this->fetchOccurrencesForProgramme($programmeEnrolInstance->customchar1);
            foreach ($occurrences as $occurrence) {
                $this->fetchEnrolmentsForOccurrence($occurrence->EnrolmentId, $programmeEnrolInstance->courseid);
            }
        }

        $qualificationEnrolInstances = $DB->get_records('enrol', array('enrol' => 'vettrak', 'status' => 0, 'customchar2' => 'qualification'));

        foreach ($qualificationEnrolInstances as $qualificationEnrolInstance) {
            $occurrences = $this->fetchOccurrencesForQualification($qualificationEnrolInstance->customchar1);
            foreach ($occurrences as $occurrence) {
                $this->fetchEnrolmentsForOccurrence($occurrence->EnrolmentId, $qualificationEnrolInstance->courseid);
            }
        }

        // $transaction->allow_commit();

        $x = $DB->get_records('tmp_enrol_vettrak');

        if (count($x) == 0) {
            mtrace('unable to identify records, skipping');
            return true;
        }

        try {
            mtrace('Performing Missing Users');
            $this->performFindingMissingUsers();
        } catch (Exception $e) {
            mtrace('Unable to insert all missing users.' . $e->getMessage());
        }

        try {
            mtrace('Performing Enrolments');
            $this->performEnrolments();
        } catch (Exception $e) {
            mtrace('Unable to enrol all users.' . $e->getMessage());
        }

        try {
            mtrace('Performing Unenrolments');
            $this->performUnEnrolments();
        } catch (Exception $e) {
            mtrace('Unable to unenrol users.' . $e->getMessage());
        }

        try {
            mtrace('Performing Groups');
            $this->performGroups();
        } catch (Exception $e) {
            mtrace('Unable to create groups.' . $e->getMessage());
        }

        try {
            mtrace('Performing Cohorts');
            $this->performCohorts();
        } catch (Exception $e) {
            mtrace('Unable to create cohorts.' . $e->getMessage());
        }

        $dbman->drop_table($table);

    }

    public function performFindingMissingUsers() {
        global $CFG, $DB;

        $usersToCreate = $DB->get_records_sql("SELECT DISTINCT clientcode FROM mdl_tmp_enrol_vettrak WHERE clientcode NOT IN (SELECT idnumber FROM mdl_user)");

        $vettrakauth = get_auth_plugin('vettrak');

        $tmpClient = new SoapClient(auth_plugin_vettrak::vettrak_webservice());

        foreach ($usersToCreate as $cliecode => $data) {

            $client = $tmpClient->GetClientDetails(array(
                    'sToken' => $this->token,
                    'sClie_Code' => $cliecode
                )
            );

            if ($client->GetClientDetailsResult->Auth->ID == '-1') {
                mtrace('Unable to Identify User. ' . $cliecode . ' Skipping.');
                continue;
            }

            try {
                $vettrakauth->upsert_client($client);
            } catch (Exception $e) {
                mtrace('Failed to insert user...');
            }

            mtrace('Upserted Client');

        }

    }

    public function performUnEnrolments() {

        global $CFG, $DB;

        $query = "  SELECT UE.id, U.id as 'userid', E.id as 'enrolid', E.courseid as 'courseid', E.enrol as 'enrol'
                    FROM mdl_user_enrolments UE, mdl_user U, mdl_enrol E
                    WHERE UE.userid = U.id
                    AND E.id = UE.enrolid
                    AND E.enrol = 'vettrak'
                    AND U.idnumber NOT IN (SELECT EV.clientcode FROM mdl_tmp_enrol_vettrak EV WHERE EV.courseid = E.courseid)";

        $usersToUnenrol = $DB->get_records_sql($query);

        foreach ($usersToUnenrol as $utue) {

            $enrolinstance = new stdClass();
            $enrolinstance->id = $utue->enrolid;
            $enrolinstance->enrol = $utue->enrol;
            $enrolinstance->courseid = $utue->courseid;

            mtrace('unenrolling');
            print_r($utue);

            // Disabled because this code needs a safety check. If mdl_tmp_enrol_vettrak is empty it is possible to unenrol all Vettrak enrolments which we remember was very bad.

            $this->unenrol_user($enrolinstance, $utue->userid);

        }

    }

    public function performCohorts() {
        global $CFG, $DB;
        
        $newCohorts = $DB->get_records_sql("SELECT DISTINCT occucode FROM {tmp_enrol_vettrak} WHERE occucode NOT IN (SELECT name FROM {cohort} WHERE name = occucode AND component = 'enrol_vettrak')");

        require_once($CFG->dirroot . '/cohort/lib.php');

        foreach ($newCohorts as $newCohort) {
            $newCohortObject = new stdClass();
            $newCohortObject->name = $newCohort->occucode;
            $newCohortObject->contextid = context_system::instance()->id;
            $newCohortObject->component = 'enrol_vettrak';
            mtrace('Successfully created Cohort for: ' . $newCohort->occucode);
            $newCohort = cohort_add_cohort($newCohortObject);
        }

        // Create Cohorts

        mtrace('Adding Users');

        $newCohortMembers = $DB->get_records_sql("  SELECT  
                                                            CONCAT(U.id, '-', C.id) as 'id',  
                                                            U.id as 'userid',
                                                            C.id as 'cohortid'
                                                    FROM mdl_tmp_enrol_vettrak T, mdl_cohort C, mdl_user U
                                                    WHERE T.occucode = C.name
                                                    AND C.component = 'enrol_vettrak'
                                                    AND T.clientcode = U.idnumber
                                                    AND U.id NOT IN (SELECT CM.userid FROM mdl_cohort_members CM WHERE CM.id = C.id)
                                                    GROUP BY U.id, T.courseid");

        foreach ($newCohortMembers as $newCohortMember) {
            cohort_add_member($newCohortMember->cohortid, $newCohortMember->userid);
        }

        mtrace('Removing Users');

        $redundantCohortMembers = $DB->get_records_sql("SELECT
                                                                CM.id as 'id',
                                                                C.id as 'cohortid',
                                                                U.id as 'userid'
                                                        FROM mdl_cohort_members CM, mdl_cohort C, mdl_user U
                                                        WHERE
                                                        CM.cohortid = C.id
                                                        AND C.component = 'enrol_vettrak'
                                                        AND CM.userid = U.id
                                                        AND U.idnumber NOT IN (SELECT clientcode FROM mdl_tmp_enrol_vettrak WHERE occucode = C.name)
        ");

        foreach ($redundantCohortMembers as $redundantCohortMember) {
            cohort_remove_member($redundantCohortMember->cohortid, $redundantCohortMember->userid);
        }

    }

    public function performGroups() {
        global $CFG, $DB;
        $groupsQuery = "    SELECT EV.id, U.id as userid, E.courseid as 'courseid', EV.occucode
                            FROM {tmp_enrol_vettrak} EV, {user} U, {enrol} E
                            WHERE U.idnumber = EV.clientcode
                            AND U.auth = 'vettrak'
                            AND E.courseid = EV.courseid
                            AND E.enrol = 'vettrak'
                            AND E.status = 0";

        $userstoGroup = $DB->get_records_sql($groupsQuery);

        foreach ($userstoGroup as $utg) {
            enrol_vettrak_plugin::vettrak_enrol_assign_group($utg->userid, $utg->courseid, $utg->occucode);
        }
    }

    public function performEnrolments($resyncEnrolments = true) {
        global $CFG, $DB;

        $resyncEnrolments = @$CFG->vettrak_resyncEnrolments;
        // $resyncEnrolments = true;

        // print_r($resyncEnrolments);
        // die('yo');

        $CFG->recovergradesdefault = true; // Force Grades to Recover

        $plugin = enrol_get_plugin('vettrak');

        $enrolmentsToDoQuery = '';

        $config = get_config('enrol_vettrak');

        if ($resyncEnrolments) { 
            // die('yes');
            $enrolmentsToDoQuery = " SELECT EV.id as 'id',
                        U.id as userid,
                        E.id as 'enrolid',
                        E.enrol as 'enrol',
                        E.courseid as 'courseid',
                        EV.startdate as 'enrolmentstart',
                        EV.enddate as 'enrolmentend'
            FROM {tmp_enrol_vettrak} EV, {user} U, {enrol} E
            WHERE U.idnumber = EV.clientcode
            AND U.auth = 'vettrak'
            AND E.courseid = EV.courseid
            AND E.enrol = 'vettrak'";

        } else {
 // die('no');
            $enrolmentsToDoQuery = " SELECT EV.id as 'id',
                        U.id as userid,
                        E.id as 'enrolid',
                        E.enrol as 'enrol',
                        E.courseid as 'courseid',
                        EV.startdate as 'enrolmentstart',
                        EV.enddate as 'enrolmentend'
            FROM {tmp_enrol_vettrak} EV, {user} U, {enrol} E
            WHERE U.idnumber = EV.clientcode
            AND U.auth = 'vettrak'
            AND E.courseid = EV.courseid
            AND E.enrol = 'vettrak'
            AND E.status = 0
            AND U.id NOT IN (   SELECT UE.userid
                                FROM {user_enrolments} UE
                                WHERE UE.userid = U.id
                                AND UE.enrolid = E.id
                                AND UE.timestart = EV.startdate
                                AND UE.timeend = EV.enddate
            )";

        }

        $enrolmentsToDo = $DB->get_records_sql($enrolmentsToDoQuery);

        foreach ($enrolmentsToDo as $enrolmentToDo) {
            $enrolinstance = new stdClass();
            $enrolinstance->id = $enrolmentToDo->enrolid;
            $enrolinstance->enrol = $enrolmentToDo->enrol;
            $enrolinstance->courseid = $enrolmentToDo->courseid;

            if (@$config->useStartDate == 0) {
                $enrolmentToDo->enrolmentstart = 0;
            }

            if (@$config->useEndDate == 0) {
                $enrolmentToDo->enrolmentend = 0;
            }


            $plugin->enrol_user($enrolinstance, $enrolmentToDo->userid, 5, $enrolmentToDo->enrolmentstart, $enrolmentToDo->enrolmentend);
        }

    }

    public function fetchOccurrencesForQualification($qualificationcode) {
        $tmpClient = $this->tmpClient;
        $additionaldata = $tmpClient->QueryAdditionalData(
                        array(
                            'token' => $this->token, // QueryAdditionalData - EnrolledUnit
                            'entityName' => 'Enrolment',
                            'filterCriteria' => array(
                                array(
                                    'Field' => 'QualificationCode',
                                    'Operator' => 'Equals', 
                                    'Value' => $qualificationcode
                                ),
                                array(
                                    'Field' => 'ClientCode',
                                    'Operator' => 'Equals',
                                    'Value' => ''
                                ),
                                array(
                                    'Field' => 'Status',
                                    'Operator' => 'Equals',
                                    'Value' => 'Active (Commencement)'
                                )
                            )
                        )
        );
        $data = array();
        if ($additionaldata->QueryAdditionalDataResult->Auth->StatusMessage == '1 records returned') {
            $row = $additionaldata->QueryAdditionalDataResult->Values->ArrayOfString;
            $additionaldata->QueryAdditionalDataResult->Values->ArrayOfString = array($row);
        }
        foreach(@$additionaldata->QueryAdditionalDataResult->Values->ArrayOfString as $key => $value) {
            $tobj = new stdClass();
            foreach ($value->string as $vkey => $vvalue) {
                $tobj->{$additionaldata->QueryAdditionalDataResult->Fields->string[$vkey]} = $vvalue;
            }
            $data[$key] = $tobj;
        }
        return $data;
    }

    public function fetchOccurrencesForProgramme($programmecode) {
        $tmpClient = $this->tmpClient;
        
        $additionaldata = $tmpClient->QueryAdditionalData(
                        array(
                            'token' => $this->token, // QueryAdditionalData - EnrolledUnit
                            'entityName' => 'Enrolment',
                            'filterCriteria' => array(
                                array(
                                    'Field' => 'ProgrammeId',
                                    'Operator' => 'Equals',
                                    'Value' => $programmecode
                                ),
                                array(
                                    'Field' => 'ClientCode',
                                    'Operator' => 'Equals',
                                    'Value' => ''
                                ),
                            )
                        )
        );

        $data = array();

        if ($additionaldata->QueryAdditionalDataResult->Auth->StatusMessage == '1 records returned') {
            $row = $additionaldata->QueryAdditionalDataResult->Values->ArrayOfString;
            $additionaldata->QueryAdditionalDataResult->Values->ArrayOfString = array($row);
        } else if ($additionaldata->QueryAdditionalDataResult->Auth->StatusMessage == '0 records returned') {
            return $data;
        }

        foreach(@$additionaldata->QueryAdditionalDataResult->Values->ArrayOfString as $key => $value) {
            $tobj = new stdClass();
            foreach ($value->string as $vkey => $vvalue) {
                $tobj->{$additionaldata->QueryAdditionalDataResult->Fields->string[$vkey]} = $vvalue;
            }
            $data[$key] = $tobj;
        }

        return $data;
    }

    public function add_course_navigation($instancesnode, stdClass $instance) {
        if ($instance->enrol !== 'vettrak') {
             throw new coding_exception('Invalid enrol instance type!');
        }
        $context = context_course::instance($instance->courseid);
        if (has_capability('enrol/vettrak:config', $context)) {
            $managelink = new moodle_url('/enrol/vettrak/edit.php', array('courseid'=>$instance->courseid));
            $instancesnode->add($this->get_instance_name($instance), $managelink, navigation_node::TYPE_SETTING);
        }
    }

    public function get_instance_name($instance) {
        return get_string('pluginname', 'enrol_vettrak');
    }

    public function get_newinstance_link($courseid) {
        global $DB;
        if ($DB->get_record('enrol', array('enrol' => 'vettrak', 'courseid' => $courseid))) { // Singleton vettrak Instance - Rely on Course IDNUMBER / SHORTNAME for now...
            return NULL;
        }
        if (!$this->can_add_new_instances($courseid)) {
            return NULL;
        }
        return new moodle_url('/enrol/vettrak/edit.php', array('courseid'=>$courseid));
    }

    public function instance_deleteable($instance) {
        return true;
    }

    public function add_default_instance($course) {
        $fields = array(
            'status'          => 0,
            'roleid'          => 5,
            'enrolperiod'     => 0,
            'expirynotify'    => 0,
            'notifyall'       => 0,
            'expirythreshold' => 0,
        );
        return $this->add_instance($course, $fields);
    }

    public function add_instance($course, array $fields = NULL) {
        global $DB;
        if ($DB->record_exists('enrol', array('courseid'=>$course->id, 'enrol'=>'vettrak'))) {
            return NULL;
        }
        return parent::add_instance($course, $fields);
    }

    protected function can_add_new_instances($courseid) {
        global $DB;
        $coursecontext = context_course::instance($courseid);
        if (!has_capability('moodle/course:enrolconfig', $coursecontext) or !has_capability('enrol/vettrak:config', $coursecontext)) {
            return false;
        }
        return true;
    }

    public static function vettrak_enrol_assign_group($userid, $courseid, $groupname) {
        global $CFG, $DB;
        require_once($CFG->libdir . '/grouplib.php');
        require_once($CFG->dirroot.'/group/lib.php');
        $ExistingGroup = $DB->get_record('groups', array('courseid' => $courseid, 'name' => $groupname));
        if (!$ExistingGroup) {
            $data = new stdClass();
            $data->courseid = $courseid;
            $data->idnumber = '';
            $data->name = $groupname;
            $data->description = 'Automatically generated group from vettrak Integration';
            $data->descriptionformat = FORMAT_HTML;
            $id = groups_create_group($data);
            $data->id = $id;
            $ExistingGroup = $data;
        }
        groups_add_member($ExistingGroup, $userid, 'enrol_vettrak');
    }

    public function get_action_icons(stdClass $instance) {
        global $OUTPUT;

        if ($instance->enrol !== 'vettrak') {
            throw new coding_exception('invalid enrol instance!');
        }
        $context = context_course::instance($instance->courseid);

        $icons = array();

        if (has_capability('enrol/vettrak:config', $context)) {
            $editlink = new moodle_url("/enrol/vettrak/edit.php", array('courseid'=>$instance->courseid, 'id'=>$instance->id));
            $icons[] = $OUTPUT->action_icon($editlink, new pix_icon('t/edit', get_string('edit'), 'core',
                    array('class' => 'iconsmall')));
        }
        return $icons;
    }

    public function allow_unenrol_user(stdClass $instance, stdClass $ue) {
        // Preserve Cohort Behaviour
        if ($ue->status == ENROL_USER_SUSPENDED) {
            return true;
        }
        return false;
    }

    public function get_user_enrolment_actions(course_enrolment_manager $manager, $ue) {
        $actions = array();
        $context = $manager->get_context();
        $instance = $ue->enrolmentinstance;
        $params = $manager->get_moodlepage()->url->params();
        $params['ue'] = $ue->id;
        if ($this->allow_unenrol_user($instance, $ue) && has_capability('enrol/vettrak:unenrol', $context)) {
            $url = new moodle_url('/enrol/unenroluser.php', $params);
            $actions[] = new user_enrolment_action(new pix_icon('t/delete', ''), get_string('unenrol', 'enrol'), $url, array('class'=>'unenrollink', 'rel'=>$ue->id));
        }
        return $actions;
    }

}
