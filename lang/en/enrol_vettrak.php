<?php

$string['pluginname'] = 'Vettrak Enrolment';
$string['pluginname_desc'] = 'Vettrak Automatic Enrolment Plugin';

// Permissions Page:
$string['vettrak:config'] = 'Configure Vettrak instances';
$string['vettrak:unenrol'] = 'Unenrol suspended users';

$string['sResult_Name'] = 'Competent';

// Edit Page
$string['status'] = 'Status';
$string['vettrakidentifier'] = 'Vettrak Code';
$string['assignrole'] = 'Role';
$string['addgroup'] = 'Group';

// Scheduled Tasks
$string['fetch_programmes'] = 'Fetch Programmes Cache';
$string['fetch_occurrences'] = 'Fetch Occurrences Cache';
$string['fetch_qualifications'] = 'Fetch Qualifications Cache';
$string['push_grades'] = 'Push Unsychronised Grades';
$string['fetch_enrolments'] = 'Synchronise Vettrak Enrolments with Moodle Courses';

$string['status_desc'] = 'Allow course access of internally enrolled users. This should be kept enabled in most cases.';