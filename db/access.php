<?php

defined('MOODLE_INTERNAL') || die();

$capabilities = array(

    // enrol/xxx:config - Implemented when plugin allows user to modify instance properties. Automatic synchronisation plugins do not usually need this capability.

    'enrol/vettrak:config' => array(

        'captype' => 'write',
        'contextlevel' => CONTEXT_COURSE,
        'archetypes' => array(
            'editingteacher' => CAP_ALLOW,
            'manager' => CAP_ALLOW,
        )
    ),
    'enrol/vettrak:unenrol' => array(

        'captype' => 'write',
        'contextlevel' => CONTEXT_COURSE,
        'archetypes' => array(
            'manager' => CAP_ALLOW,
        )
    ),

);
