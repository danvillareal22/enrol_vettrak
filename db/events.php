<?php

$observers = array(
    array(
        'eventname'   => 'core\event\user_graded',
        'callback'    => 'enrol_vettrak_user_graded',
        'includefile' => 'enrol/vettrak/lib.php'
    )
);