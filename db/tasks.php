<?php

$tasks = array(                                                                                                                     
    array(                                                                                                                          
        'classname' => 'enrol_vettrak\task\fetch_occurrences',                                                                            
        'blocking' => 1,                                                                                                            
        'minute' => '20',                                                                                                            
        'hour' => '02',                                                                                                              
        'day' => '*',                                                                                                               
        'dayofweek' => '*',                                                                                                         
        'month' => '*'                                                                                                              
    ),
	array(                                                                                                                          
        'classname' => 'enrol_vettrak\task\fetch_programmes',                                                                            
        'blocking' => 1,                                                                                                            
        'minute' => '20',                                                                                                            
        'hour' => '03',                                                                                                              
        'day' => '*',                                                                                                               
        'dayofweek' => '*',                                                                                                         
        'month' => '*'                                                                                                              
    ),
	array(                                                                                                                          
        'classname' => 'enrol_vettrak\task\fetch_qualifications',                                                                            
        'blocking' => 1,                                                                                                            
        'minute' => '20',                                                                                                            
        'hour' => '04',                                                                                                              
        'day' => '*',                                                                                                               
        'dayofweek' => '*',                                                                                                         
        'month' => '*'                                                                                                              
    ),
	array(                                                                                                                          
        'classname' => 'enrol_vettrak\task\fetch_enrolments',                                                                            
        'blocking' => 1,                                                                                                            
        'minute' => '15',                                                                                                            
        'hour' => '*',                                                                                                              
        'day' => '*',                                                                                                               
        'dayofweek' => '*',                                                                                                         
        'month' => '*'                                                                                                              
    ),
	array(                                                                                                                          
        'classname' => 'enrol_vettrak\task\push_grades',                                                                            
        'blocking' => 1,                                                                                                            
        'minute' => '30',                                                                                                            
        'hour' => '*',                                                                                                              
        'day' => '*',                                                                                                               
        'dayofweek' => '*',                                                                                                         
        'month' => '*'                                                                                                              
    )
);