<?php

defined('MOODLE_INTERNAL') || die();

require_once("$CFG->libdir/formslib.php");

class enrol_vettrak_edit_form extends moodleform {

    function definition() {
        global $CFG, $DB;

        $mform  = $this->_form;

        list($instance, $plugin, $course) = $this->_customdata;
        $coursecontext = context_course::instance($course->id);

        $enrolmenttypeselected = 'programme';
        $programmeselected = false;
        $occuselected = array();

        if ($instance->id) {
            $d = $DB->get_record_sql("SELECT * FROM {enrol} E WHERE E.id = ?", array($instance->id));
            if ($d->customchar2 == 'occurrence') {
                $occuselected = explode(',', $d->customchar1);
            } else if ($d->customchar2 == 'programme') {
                $programmeselected = $d->customchar1;
            } else if ($d->customchar2 == 'qualification') {
                $qualificationSelected = $d->customchar1;
            }
            $enrolmenttypeselected = $d->customchar2;
        }

        $enrol = enrol_get_plugin('vettrak');

        // $groups = array(0 => get_string('none'));
        // foreach (groups_get_all_groups($course->id) as $group) {
        //     $groups[$group->id] = format_string($group->name, true, array('context'=>$coursecontext));
        // }

        $mform->addElement('header','general', get_string('pluginname', 'enrol_vettrak'));

        // $mform->addElement('text', 'name', get_string('custominstancename', 'enrol'));
        // $mform->setType('name', PARAM_TEXT);

        $options = array(ENROL_INSTANCE_ENABLED  => get_string('yes'),
                         ENROL_INSTANCE_DISABLED => get_string('no'));
        $mform->addElement('select', 'status', get_string('status', 'enrol_vettrak'), $options);

        $enrolmenttype = array(
            'programme'  => 'Programme',
            'qualification' => 'Qualification',
            'occurrence' => 'Occurrences'  
        );

        $enrolmenttype = $mform->addElement('select', 'enrolmenttype', 'Enrolment Type', $enrolmenttype);
        $enrolmenttype->setSelected($enrolmenttypeselected);

        $qualifications = $enrol->fetchQualifications();

        $qualopts = array();

        foreach ($qualifications as $qualification) {

            if (empty($qualification['Qual_Code'])) {
                // $propts[$programme['Prog_Code']] = '[' . 'No Code' . '] ' . $programme['Prog_Name'];
            } else {
                $qualopts[$qualification['Qual_Code']] = '(' . $qualification['Qual_Code'] . ') ' . $qualification['Qual_Name'];
            }
            
        }

        asort($qualopts);

        $occurrences = $enrol->fetchOccurrenceOptions();
        // $occurrences = array();

        $programmes = $enrol->fetchProgrammes();

        $propts = array();

        foreach ($programmes as $programme) {

            if (empty($programme['Prog_Code'])) {
                $propts[$programme['Prog_ID']] = $programme['Prog_Name'];
            } else {
                $propts[$programme['Prog_ID']] = '(' . $programme['Prog_Code'] . ') ' . $programme['Prog_Name'];
            }
            
        }

        asort($propts);

        $pgms = $mform->addElement('select', 'programme', 'Programme', $propts);
        $mform->setDefault('programme', $programmeselected);

        $mform->disabledIf('programme', 'enrolmenttype', 'neq', 'programme');

        // $mform->addElement('text', 'customchar1', get_string('vettrakidentifier', 'enrol_vettrak')); // Change to Char...
        $mform->setType('programme', PARAM_RAW);

        $qual = $mform->addElement('select', 'qualification', 'Qualifications', $qualopts);
        $mform->setDefault('qualification', @$qualificationSelected);
        $mform->disabledIf('qualification', 'enrolmenttype', 'neq', 'qualification');

        // $mform->addElement('text', 'customchar1', get_string('vettrakidentifier', 'enrol_vettrak')); // Change to Char...
        $mform->setType('qualification', PARAM_RAW);

        $occu = $mform->addElement('select', 'occurrences', 'Occurrences', $occurrences);
        $occu->setMultiple(true);
        $occu->setSelected($occuselected);
        $mform->disabledIf('occurrences', 'enrolmenttype', 'neq', 'occurrence');

        // $mform->addElement('text', 'customchar1', get_string('vettrakidentifier', 'enrol_vettrak')); // Change to Char...
        $mform->setType('occurrences', PARAM_RAW);

        // $mform->addElement('text', 'customchar2', 'Limit Divisions'); // Change to Char...
        // $mform->setType('customchar2', PARAM_TEXT);

        // $roles = get_assignable_roles($coursecontext);
        // $roles[0] = get_string('none');
        // $roles = array_reverse($roles, true); // Descending default sortorder.
        // $mform->addElement('select', 'roleid', get_string('assignrole', 'enrol_vettrak'), $roles);
        // $mform->setDefault('roleid', $enrol->get_config('roleid'));
        // if ($instance->id and !isset($roles[$instance->roleid])) {
        //     if ($role = $DB->get_record('role', array('id'=>$instance->roleid))) {
        //         $roles = role_fix_names($roles, $coursecontext, ROLENAME_ALIAS, true);
        //         $roles[$instance->roleid] = role_get_name($role, $coursecontext);
        //     } else {
        //         $roles[$instance->roleid] = get_string('error');
        //     }
        // }
        // $mform->addElement('select', 'customint2', get_string('addgroup', 'enrol_vettrak'), $groups);

        $mform->addElement('hidden', 'courseid', null);
        $mform->setType('courseid', PARAM_INT);

        $mform->addElement('hidden', 'id', null);
        $mform->setType('id', PARAM_INT);

        if ($instance->id) {
            $this->add_action_buttons(true);
        } else {
            $this->add_action_buttons(true, get_string('addinstance', 'enrol'));
        }

        $this->set_data($instance);
    }

    function validation($data, $files) {

        global $DB;

        $errors = parent::validation($data, $files);

        // $params = array('roleid'=>$data['roleid'], 'customchar1'=>$data['customchar1'], 'courseid'=>$data['courseid'], 'id'=>$data['id']);
        // if ($DB->record_exists_select('enrol', "roleid = :roleid AND customchar1 = :customchar1 AND courseid = :courseid AND enrol = 'vettrak' AND id <> :id", $params)) {
        //     $errors['roleid'] = get_string('instanceexists', 'enrol_cohort');
        // }

        return $errors;
    }
}