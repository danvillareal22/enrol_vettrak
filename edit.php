<?php

require('../../config.php');
require_once("$CFG->dirroot/enrol/vettrak/edit_form.php");
require_once("$CFG->dirroot/enrol/vettrak/lib.php");
require_once("$CFG->dirroot/group/lib.php");

$courseid = required_param('courseid', PARAM_INT);
$instanceid = optional_param('id', 0, PARAM_INT);

$course = $DB->get_record('course', array('id'=>$courseid), '*', MUST_EXIST);
$context = context_course::instance($course->id, MUST_EXIST);

require_login($course);
require_capability('moodle/course:enrolconfig', $context);
require_capability('enrol/vettrak:config', $context);

$PAGE->set_url('/enrol/vettrak/edit.php', array('courseid'=>$course->id, 'id'=>$instanceid));
$PAGE->set_pagelayout('admin');

$returnurl = new moodle_url('/enrol/instances.php', array('id'=>$course->id));
if (!enrol_is_enabled('vettrak')) {
    redirect($returnurl);
}

$enrol = enrol_get_plugin('vettrak');

if ($instanceid) {
    $instance = $DB->get_record('enrol', array('courseid'=>$course->id, 'enrol'=>'vettrak', 'id'=>$instanceid), '*', MUST_EXIST);
} else {
    // No instance yet, we have to add new instance.
    if (!$enrol->get_newinstance_link($course->id)) {
        redirect($returnurl);
    }
    navigation_node::override_active_url(new moodle_url('/enrol/instances.php', array('id'=>$course->id)));
    $instance = new stdClass();
    $instance->id         = null;
    $instance->courseid   = $course->id;
    $instance->enrol      = 'vettrak';
    $instance->customint1 = ''; // Cohort id.
    $instance->customint2 = 0;  // Optional group id.
}

// Try and make the manage instances node on the navigation active.
$courseadmin = $PAGE->settingsnav->get('courseadmin');
if ($courseadmin && $courseadmin->get('users') && $courseadmin->get('users')->get('manageinstances')) {
    $courseadmin->get('users')->get('manageinstances')->make_active();
}


$mform = new enrol_vettrak_edit_form(null, array($instance, $enrol, $course));

if ($mform->is_cancelled()) {
    redirect($returnurl);

} else if ($data = $mform->get_data()) {

    $customchar1 = '';

    if ($data->enrolmenttype == 'occurrence') {
        $customchar1 = implode(',', $data->occurrences);
    } else if ($data->enrolmenttype == 'qualification') {
        $customchar1 = $data->qualification;
    } else {
        $customchar1 = $data->programme;
    }

    if ($data->id) {

        // NOTE: no cohort changes here!!!
        // if ($data->roleid != $instance->roleid) {
        //     // The sync script can only add roles, for perf reasons it does not modify them.
        //     role_unassign_all(array('contextid'=>$context->id, 'roleid'=>$instance->roleid, 'component'=>'enrol_vettrak', 'itemid'=>$instance->id));
        // }

        $instance->name         = 'Vettrak Sync';
        $instance->status       = $data->status;
        $instance->roleid       = 5;
        $instance->customint2   = 0;
        $instance->customchar1   = $customchar1;
        $instance->customchar2   = $data->enrolmenttype;
        $instance->timemodified = time();

        $DB->update_record('enrol', $instance);

    }  else {

        $instance = array(
            'name'=> 'Vettrak Sync',
            'status'=>$data->status,
            'customchar1'=> $customchar1,
            'customchar2'=> $data->enrolmenttype,
            'roleid'=> 5,
            'customint2'=> 0
        );

        $enrol->add_instance($course, $instance);

    }

    // Process New Data

    $syncEnrolments = new adhoc_sync_enrolments();
    $syncEnrolments->set_blocking(true);
    \core\task\manager::queue_adhoc_task($syncEnrolments);

    // Process Data when it is added... I think we should enqueue this...

    // $trace = new null_progress_trace();
    // enrol_cohort_sync($trace, $course->id); // This would also become a part of the cron task...
    // $trace->finished();

    redirect($returnurl);
}

$PAGE->set_heading($course->fullname);
$PAGE->set_title(get_string('pluginname', 'enrol_vettrak'));

$PAGE->requires->jQuery();

echo $OUTPUT->header();
$mform->display();
?>
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.full.js"></script>
<style>
    form select, form input[type="text"], form input[type="password"], form.atto_form input[type="url"], select.singleselect {
        max-width: 800px !important;
    }
    body#page-enrol-vettrak-edit #id_occurrences {
        width: 800px !important;
    }
    body#page-enrol-vettrak-edit #id_programme {
        width: 800px !important;
    }
    body#page-enrol-vettrak-edit #id_qualification {
        width: 800px !important;
    }
</style>
<script>
    $('body#page-enrol-vettrak-edit #id_occurrences').select2();
    $('body#page-enrol-vettrak-edit #id_programme').select2();
    $('body#page-enrol-vettrak-edit #id_qualification').select2();
</script>
<?php
echo $OUTPUT->footer();