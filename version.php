<?php

defined('MOODLE_INTERNAL') || die();

$plugin->version   = 2014111010;        // The current plugin version (Date: YYYYMMDDXX)
$plugin->requires  = 2013110500;        // Requires this Moodle version
$plugin->component = 'enrol_vettrak';    // Full name of the plugin (used for diagnostics)
$plugin->cron      = 86400;
$plugin->dependencies = array(
    'auth_vettrak' => ANY_VERSION
);